This repository describes the Drupal community's governance process, structure, and policies. If you're interested in specific issues regarding governance, take a look in the [issue queue](http://drupal.org/project/issues/governance) and add more if you'd like. 

### Values and Principles

The Drupal Values and Principles describe the culture and behaviors expected of members of the Drupal community to uphold. These principles inform how technical and non-technical decisions are made, in addition to how contributors and leaders can support both our community and the project. [Read about our values and principles](values-and-principles.md).

### Governance structure

We have a number of working groups related to various aspects of the community: [Drupal core](core/drupal-core.md), the [Security Working Group](charters/security-working-group-charter.md), the [Technical Working Group](charters/technical-working-group-charter.md), and the [Documentation Working Group](charters/documentation-working-group-charter.md), with the Project Lead as the final decision-maker:

```mermaid
flowchart LR
    A[Project Lead] --> B(Drupal Core)
    A --> C(Security Working Group)
    A --> D(Technical Working Group)
    A --> E(Documentation Working Group)
```

In the case of certain aspects of the community that fall under the Drupal Association's mission, however, it makes sense for this final decision-maker to be the Drupal Association, since they are ultimately responsible for the website, events, legal matters, etc. These include the [Community Working Group](charters/community-working-group-charter.md), the [Drupal.org Content Working Group](charters/drupalorg-content-working-group-charter.md), the [Drupal.org Infrastructure Working Group](charters/drupalorg-infrastructure-working-group-charter.md), the [Drupal.org Software Working Group](charters/drupalorg-software-working-group-charter.md), the [Event Organizers Working Group](charters/event-organizers-working-group.md) and the [Licensing Working Group](charters/licensing-working-group-charter.md).

```mermaid
flowchart LR
    A[Drupal Association] --> B(Community Working Group)
    A --> C(Drupal.org Content Working Group)
    A --> D(Drupal.org Infrastructure Working Group)
    A --> E(Drupal.org Software Working Group)
    A --> F(Event Organizers Working Group)
    A --> G(Licensing Working Group)
```
