## Community Working Group Charter

### Mission

The mission of the Community Working Group (CWG) is to foster a friendly and welcoming community for the Drupal project and to uphold the [Drupal Code of Conduct](https://drupal.org/dcoc).

The CWG acts as a *group* to maintain documentation and develop processes and initiatives focused on the health of the community.

In cases of conflict, the CWG guides community members through the conflict resolution process in an effort to help defuse tense situations and keep discussions productive. In rare cases of intractable conflicts, they act as a point of escalation, mediation, and/or final arbitration for the Drupal community.

### Scope / duties / responsibilities

#### Scope

The CWG charter applies to situations involving Drupal community members, which includes (but is not limited to) anyone who participates in the Drupal project by: i) creating and maintaining a Drupal.org website account, ii) posting on \*.Drupal.org web sites, iii) participating in Drupal community chat channels, or iv) attending international, regional, or local Drupal events. This definition extends to any Drupal-related participation regardless of medium (e.g., Drupal.org, social media, community chat) or location (e.g., local meetup or camp, DrupalCon).

In addition to facilitating discussion and providing conflict mediation assistance, the CWG is also empowered to take action in response to community conflicts where individuals/groups can not reach a resolution on their own; for example, i) issuing binding arbitration, ii) issuing warnings, iii) temporarily banning someone from particular aspects of community participation, or iv) enforcing permanent bans (in extreme cases).

#### Duties

The CWG exists to foster a friendly and welcoming community for the Drupal project. They do so by engaging in one or more of the following activities:

1. **Advice and consultation**: The CWG acts as a resource for the Drupal project on matters relating to community health. They share knowledge, raise awareness, and promote engagement on issues that impact the Drupal community.
1. **Community initiatives**: The CWG works to develop and/or support community initiatives that promote the health of the Drupal community and help to prevent conflict and burnout.
1. **Facilitation and Documentation**: IThe CWG enables community members to resolve conflicts by guiding them through an established conflict resolution process. The CWG maintains the Drupal Code of Conduct and related documentation, including the conflict resolution process.
1. **Mediation**: When presented with a conflict that cannot be resolved by the individuals involved using the established conflict resolution process, the CWG may serve as a mediator or provide mediation recommendations.
1. **Arbitration**: If necessary, the CWG is empowered to enforce binding arbitration and/or decide on appropriate courses of action.
1. **Escalation**: In extreme cases which could result in disciplinary actions extending beyond the CWG's mandate and/or potentially into the legal realm, the CWG may provide educational and advisory support to community members regarding potential escalation alternatives.

#### Exclusions

Items specifically not within the scope of the CWG's duties:

- The CWG does not respond to requests to take specific punitive action against other community members.
- Members of the CWG cannot arbitrate conflicts individually unless empowered to do so by the group as a whole.
- It is not the role of the CWG to initiate contact with law enforcement on behalf of any of the community members involved in an incident brought before the CWG.
- The CWG cannot make technical policy decisions (this is the responsibility of the Technical Working Group) or community-wide governance decisions.
- The CWG cannot change or extend its own charter.

### Process

#### Resolving community conflicts

The CWG maintains processes and documentation to enable community members to handle and/or report violations of the Drupal Code of Conduct and/or conflicts between members of the community to the group. The CWG will work with those involved to resolve the issue following the conflict resolution process and/or take additional action as necessary to address violations of the [Drupal Code of Conduct](https://drupal.org/dcoc)<a>.</a>

#### Conflict of Interest

CWG members may decline to get involved with mediating or arbitrating a conflict if they feel that their involvement could pose a conflict of interest or may expose them to danger. The CWG will maintain and uphold a Code of Ethics that outlines expectations of members with regard to confidentiality and conflicts of interest.

#### Transparency

The CWG aims to be as transparent as possible by deliberating and documenting its activities publicly when able. In situations involving conflicts between community members, however, the group may omit details out of respect for the privacy of all the individuals involved.

#### Oversight

The CWG is overseen by a three-person Review Panel consisting of the two community-elected Drupal Association board members plus an independent representative from a different open source project that is appointed by the Drupal Association board:

- The Review Panel is not involved in the CWG's day-to-day activities; only matters that are brought to it as part of the appeals process, or at the discretion of the CWG.
- The Review Panel will always consist of three voting members, although the review panel may choose to bring in additional non-voting consultants to serve in an advisory capacity at its discretion.
- Should one of the community-elected board members be unwilling or unable to serve on the Review Panel , any vacancies will be filled by the Drupal Association board until the next community board member election. The Drupal Association board will also be responsible for appointing the independent representative and their replacement as appropriate.
- All Review Panel members must agree to support and uphold Drupal's Values and Principles, and the the Drupal Code of Conduct. All Review Panel members and consultants must also agree to abide by the CWG's Code of Ethics. If one or more Review Panel members must recuse themselves due to a conflict of interest, the Drupal Association board may appoint a temporary replacement.

While the CWG does not report to any member of Drupal Association staff, it may meet with staff members and involve them in issues as necessary and appropriate. This includes timely referral of issues brought to either the CWG or Drupal Association staff that should be addressed by the other body.

#### Appeals

If any of the involved parties feels a decision of the CWG is unreasonable, they can escalate it to the Review Panel for appeal. When an appeal is made:

- The Review Panel will review all relevant information relating to the issue, during which time the decision of the CWG stands.
- The Review Panel can:
    - Choose to override the CWG's decision.
    - Chose to uphold the CWG's decision.
    - Choose not to review the CWG's decision (in which case the CWG's decision would stand).
- The Review Panel will provide an anonymized summary of any issue that is brought
- to them, and their decision thereof, to the full Drupal Association Board. The Drupal Association board may involve itself in the issue if they feel there is a significant impact to the project, but otherwise will not have access to any confidential materials relating to any CWG matters.

#### Membership

CWG members are appointed by the Review Panel, with the advice and consultation of the existing members of the group. A member may serve up to two 3-year terms (6 years total). A list of current members will be maintained on drupal.org.

The Review Panel also has the authority to remove a CWG member in case of a violation of its code of ethics or other misconduct.

The CWG may also appoint subject matter experts as necessary to assist with matters before the CWG. Examples of subject matter experts may include people with expertise in mental health issues, legal professionals, or people with knowledge of specific languages or cultures. Subject matter experts are not considered full members of the group, but are held to the same standards as regular members and expected to abide by the CWG's Code of Ethics.

#### Charter revisions

The charter will be revised as needed. Any proposed charter revisions must be ratified by the board of the Drupal Association prior to acceptance into this charter. In the future, this charter may be revised to modify the charter revision process, subject to the aforementioned condition.
