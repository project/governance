## Documentation Working Group Charter

### Mission

The mission of the Documentation Working Group (DocWG) is to ensure that Drupal Core and Drupal’s contributed project ecosystem have excellent guidelines, policies, standards, tools, and processes in place for on-line documentation on Drupal.org. The DocWG acts as a *group* to maintain the guidelines and processes for documentation, and to recommend and guide development of tools and infrastructure that are needed for documentation.

The purpose of the DocWG's activities is to enable the community to create and maintain on-line documentation for both core and contributed projects that is accurate, complete, and maintainable.

### Scope / duties / responsibilities

#### Scope

The DocWG is responsible for the guidelines, policies, standards, tools, and processes that are required to create and maintain on-line documentation for the Drupal community. The DocWG does not necessarily create or apply these instruments itself, but merely ensures that they exist, that they are well maintained, and that they are successful.

#### Specific duties of the DocWG

The DocWG exists primarily to help the community improve on-line documentation by themselves. They do so by engaging with the community in the following capacities:

1. **On-line Documentation**: The DocWG's activities only relate to the on-line documentation section(s) of Drupal.org web sites. This is currently limited to https://drupal.org/documentation (the Community Documentation section) and https://api.drupal.org (the API reference site).
1. **Documentation Processes**: The DocWG maintains documentation and processes around how the community maintains documentation, including processes for communication about documentation changes, how to work with individual project maintainers, and how on-line documentation issues get resolved.
1**Tools**: The DocWG may also develop guidelines and/or recommendations regarding the introduction of new tools, infrastructure, and technologies for the benefit of on-line documentation. Where implementation of these recommendations is dependent on Drupal Association funding, Drupal.org integration, or significant investment from the infrastructure team, the DocWG will work with these groups to evaluate feasibility of any particular option or recommendation.
1. **Coordination and collaboration**: The DocWG will work with other governance bodies on areas of common interest and overlapping responsibilities; for example, to ensure that there is a consistency of user experience and style across the documentation and non-documentation areas of drupal.org.

#### Exclusions

Items specifically *not* within the scope of the DocWG's duties:

- The DocWG is not responsible for non-documentation Drupal.org content such as general community policies, marketing collateral, etc. These would be under the purview of their respective working groups.
- Some on-line documentation, such as the API documentation on https://api.drupal.org, is generated from source code in Drupal Core and other projects. The DocWG is not responsible for coding standards, processes, or the content of this documentation. It is only responsible for the tools used to display that documentation.
- The DocWG does not control or maintain the individual modules that power the on-line documentation sections of Drupal.org sites.
- The DocWG cannot make technical policy decisions (this is the responsibility of the Technical Working Group) or community-wide governance decisions (this is the responsibility of the Governance Working Group).
- The DocWG cannot change or extend its own charter.

### Process

#### Community-proposed changes to existing policies and/or standards

Any party who feels a policy or standard maintained by the DocWG is unreasonable may propose a change to the policy in question. Generally, modification proposals should solicit discussion and support from members of the Drupal community before being brought forward to the DocWG.

Received proposals will be evaluated by the DocWG, and may be accepted, rejected, or referred back to the proposer with a request for further elaboration or community discussion on the issue. In some cases, the DocWG may refer the issue directly back to the community for further discussion before making a final decision.

#### DocWG Membership

The DocWG consists of 3-5 members of the Drupal community who are active in the documentation field, to ensure that the decisions made within the DocWG are in harmony with the people contributing to on-line documentation.

The current members are (in alphabetical order):

- Boris Doesborg ([batigolix](https://drupal.org/user/22175)) (acting as chair)
- Lee Hunter ([LeeHunter](https://drupal.org/user/117834))
- Antje Lorch ([ifrik](https://drupal.org/user/98730))
- Joe Shindelar ([eojthebrave](https://drupal.org/user/79230))

Members are appointed by Dries Buytaert and/or his designate(s).

#### Transparency and Appeals

The DocWG aims to be as transparent as possible by documenting its decisions publicly, and specifically seeking out feedback from key stakeholders (e.g., Drupal core developers, frequent documentation contributors, etc.).

Individuals who do not agree with a given DocWG decision may escalate to Dries Buytaert and/or his designate(s), who will review the decision and can choose to either uphold or alter it. In the meantime, the decision of the DocWG stands.

### Charter revisions

The charter will be revised as needed. Any proposed charter revisions must be ratified by Dries Buytaert and/or his designate(s) prior to acceptance into this charter. In the future, this charter may be revised to modify the charter revision process, subject to the aforementioned condition.
