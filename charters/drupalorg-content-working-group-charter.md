## Drupal.org Content Working Group Charter

### Mission

The mission of the Drupal.org Content Working Group (DCWG) is to make sure that the content on Drupal.org websites is compelling, well-curated, and relevant for all of its various target audiences and supports a content marketing strategy that serves site visitors and generates revenue to make Drupal.org sustainable.

The DCWG acts as a *group* to maintain the processes around product messaging, content moderation, and user experience elements of the Drupal.org websites. The DCWG is appointed by the Drupal Association. Together the Drupal Association and DCWG collaborate on strategy, priorities, and implementation.

### Scope / duties / responsibilities

#### Scope

The DCWG manages and is responsible for overall content strategy of Drupal.org websites, maintaining policies around the major content areas on Drupal.org, including the front page, case studies, marketplace, persona-centric landing pages, and Drupal Planet. They also manage the overall look and feel and voice of the website, including its information architecture and design elements.

The DCWG uses an iterative process, based on data-driven analysis, with the goal to constantly improve the content on Drupal.org and to optimize key pages for traffic, usability, and revenue.

#### Specific duties

1. **Content strategy**: Ensure content meets audience needs, facilitate a content marketing strategy that is up-to-date and relevant, and create policies around how to handle legacy content.
1. **Information architecture**: Ensure content is well organized, and manage Drupal.org website elements such as navigation, taxonomy, header/footer.
1. **Design and User experience**: Develop and maintain a consistent “look and feel" throughout all Drupal.org websites, in alignment with the Drupal brand guidelines. Extend the Drupal Style guide as needed to address new design goals and needs for the website. Establish standards on how people in various target audiences approach the site and find the information that they need.
1. **Moderation**: Maintain policies around general user-generated content (e.g. spam, unsolicited advertising, fields on registration form, account deletion policy), as well as major content areas of the site (such as Drupal Planet, Marketplace, Case Studies, etc.)
1. **Analytics and Research**: Track analytics and demographic data to determine how well or poorly the content on the site is performing so that it can be continually improved. Perform usability studies to help prioritize improvements and research on who uses Drupal.org and how they use it.

#### Exclusions

- The DCWG does not moderate technical documentation such as the [Drupal.org documentation guides](https://drupal.org/node/1168704). This is the responsibility of the Documentation Team.
- The DCWG also does not deal with policies surrounding advertising on Drupal.org; this is managed by Drupal Association.
- The DCWG doesn't uniquely define Drupal's marketing positioning and messaging. This is defined by the Branding and Marketing committee in concert with the Drupal Association, and this team ensures that it is applied consistently to the Drupal.org website.
- The DCWG cannot change or extend its own charter.

### Process

#### Collaboration

The DCWG must establish a process that uses timely decision-making and iterative improvements. The DCWG should work closely with all other working groups, as well as Drupal Association staff, and Branding and Marketing committee, in order to determine overall needs and priorities for Drupal.org content.

#### Budget

The DCWG works within the budget provided by the Drupal Association, and presents a yearly plan with cost breakdown for Board approval.

#### Transparency and Appeals

The DCWG aims to be as transparent as possible by documenting its decisions publicly.

Individuals who do not agree with a given DCWG decision may escalate to the Drupal Association staff and/or their designate(s), who will review the decision and can choose to either uphold or alter it. In the meantime, the decision of the DCWG stands.

### Membership

The current members are (in alphabetical order):

- George DeMet (gdemet)
- Jeff Eaton (eaton)
- Megan Sanicki (megansanicki)
- Roy Scholten (yoroy)
- Tatiana Ugriumova (tvn)

Members are appointed by the Drupal Association Board and/or their designate(s).

### Charter revisions

The charter will be revised as needed. Any proposed charter revisions must be ratified by the Drupal Association Board and/or their designate(s) prior to acceptance into this charter.
