## Drupal.org Infrastructure Working Group Charter

### Mission

The mission of the Drupal.org Infrastructure Working Group (DIWG) is to ensure that all of the Drupal.org websites and supporting infrastructure remain stable, performant, and able to scale with the growth and needs of our community.

The DIWG acts as a *group* to make decisions surrounding the web infrastructure aspects of the Drupal.org websites. They also collaborate and provide support to the other working groups.

### Scope / duties / responsibilities

#### Scope

The primary goal of the DIWG is to keep all the Drupal.org websites running smoothly, and to work closely with the other Drupal.org working groups to make infrastructure-level decisions. They also provide recommendations to the Drupal Association for infrastructure-related budgets and needs.

The DIWG manages and is responsible for all infrastructure-related needs of the Drupal project. This includes servers, Git repositories, mailing lists, DNS management, e-mail management, network, server access, and security. They also handle capacity planning for future growth needs of these resources.

#### Specific duties

1. **Infrastructure planning**: capacity planning, system requirements, timeline, budgets, etc. in collaboration with the Drupal Association Board of Directors and other working groups.
1. **Hardware management**: Manage the hardware that underlies the Drupal.org websites (web servers, database servers, mail servers, load balancers, network equipment, development environments, testbots, etc.).
1. **Software management**: Manage the software that underlies the Drupal.org websites (e.g. operating system, mailing list software, HTTP server, database software, FTP services, source code management systems, continuous integration tools, backups, etc.).
1. **Providers**: Manage the relationship with external infrastructure vendors/providers: e.g. hosting companies, IRC providers, etc.
1. **Processes and documentation**: Maintains the documents and processes around the hardware and software they support (e.g. security policy, inventory of all hardware and software), deployment processes.

#### Exclusions

Items specifically not within the scope of the DIWG's duties:

- The DIWG cannot make project-wide technical policy decisions (this is the responsibility of the Technical Working Group) nor Drupal.org software decisions (this is done in coordination with the Drupal.org Software Working Group).
- The DIWG cannot change or extend its own charter.

### Process

#### Budget

The DIWG works within the budget provided by the Drupal Association, and presents a yearly plan with cost breakdown for Board approval.

#### Transparency and Appeals

The DIWG aims to be as transparent as possible by documenting its decisions publicly.

Individuals who do not agree with a given DIWG decision may escalate to the Drupal Association Board and/or their designate(s), who will review the decision and can choose to either uphold or alter it. In the meantime, the decision of the DIWG stands.

### Membership

The current members are (in alphabetical order):

- Neil Drumm (drumm)
- Rudy Grigar (basic)
- Gerhard Killesreiter (killes)
- Narayan Newton (nnewton)
- Tatiana Ugriumova (tvn)

Members are appointed by the Drupal Association Board and/or their designate(s).

### Charter revisions

The charter will be revised as needed. Any proposed charter revisions must be ratified by the Drupal Association Board and/or their designate(s) prior to acceptance into this charter.
