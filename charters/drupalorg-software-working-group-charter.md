## Drupal.org Software Working Group Charter

### Mission

The mission of the Drupal.org Software Working Group (DSWG) is to provide the tools and processes for the community to make Drupal.org an extraordinary experience for evaluators, site builders, and contributors alike, and a shining example of what Drupal can do.

The DSWG acts as a *group* to define the overall technical strategy for the Drupal.org properties, to empower teams responsible for individual sub-sites and portions of Drupal.org to build and maintain this vision, and to provide them best practices, guidance, and coordination to help expedite decision-making.

### Scope / duties / responsibilities

#### Scope

The DSWG is responsible for the overall strategy, planning, architecture, development, and maintenance of the software (e.g. modules, themes, customizations) used by the Drupal.org websites. The DSWG employs a "federated model" consisting of independent teams with centralized coordination for different aspects of the Drupal.org websites.

DSWG appoints and empowers different teams, each responsible for a particular aspect of the Drupal.org websites (e.g. issue queue, a particular sub-site), with the aim to provide contributors, end users, and sponsors of Drupal.org a delightful experience that scales to fit their needs.

Each team has the authority to make decisions within their scope but the DSWG acts as a steering committee to the different teams. It provides overall guidance, collaboration among those teams, providing advice about best practices, strategic and technical direction, etc. The DSWG also works with the Drupal Association on budgeting related to each team's software needs.

#### Specific duties

The DSWG provides vision and direction for Drupal.org, and works with the community on the following aspects of the Drupal.org software:

1. **Team Leadership** Creates and removes teams for each major area of the Drupal.org websites, which have authority to make software and feature decisions within their scope. The DSWG defines and appoints the leadership roles within each team, such as a technical lead, product owner, QA lead, etc.
1. **Ideation**: Working with the different Drupal.org target audiences (e.g. contributors, evaluators, site builders, etc) to understand their needs and coordinate with the different teams on implementation.
1. **Planning**: Collaborate with the Drupal community, the Drupal Association Board of Directors, and individual teams on maintaining an up-to-date plan for the Drupal.org websites that includes a prioritized feature roadmap, costs, schedule breakdown, deliverables, resource needs, and contingency plans.
1. **Architecture**: Establish processes to ensure that we can efficiently make architectural decisions about the Drupal.org software, including those related to overall site architecture, choosing between different technical approaches, etc.
1. **Development and Maintenance**: Provide guidance and processes to ensure the smooth running of the Drupal.org websites, including: responsibility and accountability of the teams, performance, security (including roles/permissions on Drupal.org), upgrades, quality management, and sandbox environments.

#### Exclusions

Items specifically not within the scope of the DSWG’s duties:

- The DSWG, as well as its sub-teams, cannot make deployment decisions without coordination with the Drupal.org Infrastructure Working Group (DIWG).
- The DSWG, as well as its sub-teams, cannot make visual design changes to the Drupal.org websites without coordination with the Drupal.org Content Working Group (DCWG).
- The DSWG cannot change or extend its own charter.

### Process

#### Collaboration

The DSWG must establish a process that balances timely decision-making with involvement of a broad spectrum of the Drupal community. The DSWG should also work closely with all other working groups, as well as Drupal Association staff, in order to determine overall needs and priorities for Drupal.org.

#### Budget

The DSWG works within the budget provided by the Drupal Association, and presents a yearly plan with cost breakdown for Board approval.

#### Transparency and Appeals

The DSWG aims to be as transparent as possible by documenting its decisions publicly.

Individuals who do not agree with a given \*.Drupal.org decision may escalate to the DSWG, or ultimately to the Drupal Association Board of Directors and/or their designate(s). They will review the decision and can choose to either uphold or alter it. In the meantime, the decision stands.

### Membership

The current members are (in alphabetical order):

- Angela Byron (webchick)
- Neil Drumm (drumm)
- David Hernandez (davidhernandez)
- Cathy Theys (YesCT)
- Tatiana Ugriumova (tvn)

Members are appointed by the Drupal Association Board of Directors and/or their designate(s).

### Charter revisions

The charter will be revised as needed. Any proposed charter revisions must be ratified by the Drupal Association Board of Directors and/or their designate(s) prior to acceptance into this charter.
