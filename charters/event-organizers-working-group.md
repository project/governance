## Event Organizers Working Group Charter

### Mission

The Event Organizers Working Group supports community-led teams to grow Drupal through local events.

### Vision

To establish a world-wide network of Drupal experts who organize events and aid one another to do the same.

### Scope

The EOWG is concerned with supporting community-led events within the Drupal community. These include camps, summits, training days, contribution days, and meetups. While the material we produce will be open and accessible to anyone (just as much of our best practices have come from other communities), we intend to focus on producing documentation and services to support and promote community-led events.

### Duties

The EOWG exists to support new and existing Drupal events. They do so through a number of activities:

- **Data collection:** Collaborating with the Drupal Association and local associations to collect data about events for the benefit of the Drupal community.
- **Documentation:** Working to build collections of best-practices to ease the barrier to entry for new events and promote sustainability within existing events.
- **Mentorship:** Connecting event organizers to share knowledge.
- **Fellowship:** Fostering an ongoing dialogue in the community of event organizers through regular meetings and open forums.
- **Collaboration:** Connecting organizers to other Working Groups &amp; Initiatives in the community.

### Exclusions

- While we hope to collaborate with other parties, this group has no direct connection to the planning or implementation of Drupal Association or commercial events.
- While we are happy to provide information and advice, we do not plan other organizations’ events.
- We do not provide any legal advice or services to events.
- We do not provide funding to events.

### Values

#### Support Over Standardization

We recognize that the Drupal community has gained immense success through its organic, volunteer-led growth and we want in no way to impede the organic nature of such a community. The EOWG exists to support each event and community in the ways that promote best practices but in no way intend to co-opt the diverse set of events that already exist.

#### Transparency

The EOWG aims to be as transparent as possible by deliberating and documenting its activities publicly.

#### Inclusion

The EOWG works to be inclusive of all people around the world. We aim to operate in a globally-inclusive manner—rotating meeting times throughout the 24-hour day, for example—to facilitate collective participation from our global team.

### Board &amp; Advisory Committee

While we intend for contributions to the EOWG to be made by as wide a group as possible, we maintain two leadership groups to prioritize issues and provide governance.

#### Board

The EOWG board meets at least quarterly to establish and prioritize the initiatives of the group. They serve as initiative owners—managing work, allocating resources as available, and providing status updates. The Board will report to the community on its activity through the [Event Organizers Working Group blog](https://www.drupal.org/community/event-organizers/blog), and will track initiative progress through the [Event Organizers Project issue queue](https://www.drupal.org/project/issues/event_organizers).

##### Composition

The Board will be composed of seven members, four appointed and three elected\*. Board members will serve two-year terms with a limit of two consecutive terms served. After four years of service, board members will be required to take at least one year off before being eligible for another term. Board member terms will be offset so that no more than four members turn over per year, and each year will have a mix of elected and appointed members turning over.

\* The initial composition of the Board will be decided by the Formation Board, with three of the seven members serving a three-year initial term, as decided by random selection.

##### Procedures

Elections will be conducted yearly as elected positions become available. Election procedures are still to be determined.

##### Eligibility

In order to be considered for the Board, a prospective member should be engaged in Event Organizer initiatives (with appropriate credits on the issue queue) and should have attended at least 75% of public meetings in the past year.

##### Time Commitment

Board members are expected to attend or remotely contribute to all board meetings during their tenure and are expected to dedicate time toward leading initiatives. Members are expected to dedicate at least two hours per month toward these activities.

#### Advisory Committee

The EOWG Advisory Committee is appointed by the Board at their discretion. Advisors will be selected to assist or advise the board on current initiatives that require additional expertise.

Advisers may include former or prospective Board members and leaders in the Drupal community or beyond.

There are no established term limits for Advisory Committee members, but the Board is expected to appoint and release them as current initiatives dictate.

### Membership

Anyone involved in organizing an existing or future community event is welcome to participate in Event Organizer Working Group monthly meetings and contribute to initiatives as directed by the Board. We ask that participants events be publicly listed on groups.drupal.org, Meetup, or another similar location.

Involvement in meetings and initiatives will be recognized with credits on the [Event Organizers issue queue](https://www.drupal.org/project/issues/event_organizers).

### Code of Conduct

All participants in EOWG activities are expected to abide by the [Drupal Code of Conduct](https://www.drupal.org/dcoc). As per that document, any incidents should be reported to the [CWG Incident Form](https://www.drupal.org/governance/community-working-group/incident-report).

### Charter Revisions

The charter will be revised as needed. Any proposed charter revisions must be ratified by the Drupal Association and/or their designate(s) prior to acceptance into this charter. In the future, this charter may be revised to modify the charter revision process, subject to the aforementioned condition.
