## Licensing Working Group Charter

### Mission

The mission of the Licensing Working Group (LWG) is to establish and enforce policies regarding license management for code and related assets hosted on Drupal.org and to communicate those policies to the community at large. The LWG acts as a group to maintain the guidelines and processes for license policy enforcement and to recommend and guide development of tools and infrastructure that are needed for said enforcement.

### Scope / Duties / Responsibilities

#### Scope

The LWG is responsible for the guidelines, policies, standards, tools, and processes around licensing of code and related assets hosted on Drupal.org, and to enforce these policies.

#### Specific Duties

The LWG exists to be an authoritative voice in the event of license policy disagreements. They do so by engaging in the following activities:

- **Establishing and maintaining policy on the license by which code and assets are distributed from Drupal.org**. This includes both "first-party" resources (code/assets uploaded directly to Drupal.org) as well as "third-party" resources (external code/assets packaged for download on Drupal.org).
- **Maintaining the "whitelist" of allowable third-party licenses/projects**. Said whitelist (https://www.drupal.org/packaging-whitelist) will be used in projects such as Distributions that pull in external, third-party code for packaging on Drupal.org.
- **Maintaining documentation related to licensing policy**. This includes the Licensing FAQ (https://www.drupal.org/licensing/faq), policies on third-party code contributions on Drupal.org (https://www.drupal.org/node/422996), Drupal Git Repository Usage policy (https://www.drupal.org/node/1001544), etc. Create an additional document: “Contacted by the LWG. Now what?”
- **Enforcing licensing policies on contributed code/assets**. This includes responding to reports of license violation in a timely manner, providing ample notice to a contributor that they are in violation of the licensing policy and what the steps to resolution are, and where necessary taking action against a project in violation such as unpublishing it until the code is brought into compliance with the license.
- **Partnering with other working groups**, such as the Drupal.org Software/Infrastructure Working Groups, or the Documentation Working Group, on tools/policies to help with license policy enforcement that fall under the scope of those groups. Recommendations for tool/policy changes are made as a group.

#### Exclusions

Items specifically *not* within the scope of the LWG’s duties:

- Actively policing and proactively looking for license violations; the group is more responsive in nature, fielding reports that come in (which may or may not originate from group members).
- Responding to specific legal actions, such as DMCA Takedown notices. These should be directed to the Drupal Association, which is a legal entity, as opposed to a group of volunteers.
- The LWG can make recommendations as a group, but cannot make changes (e.g. GPLv2 to GPLv3) to the license by which first-party code is distributed from Drupal.org. This decision rests with the project lead, Dries Buytaert.
- Issues relating to trademark enforcement and violation (either by Drupal or Drupal modules or by 3rd parties of the Drupal trademark) are not within the scope of the LWG.
- Content on Drupal.org, which is not checked into a Git repository, is not regulated by the LWG.

### Process

#### Transparency

The LWG aims to be as transparent as possible by deliberating and documenting its decisions publicly in an issue tracker. The group will meet on a recurring basis to review new reports that have come in and take any action required.

Any proposed policy adjustments that affect a non-trivial number of contributors will be as widely publicized as possible and include a public "request for comment" period.

#### Community-proposed changes to existing policies and/or standards

Any party who feels a policy or standard maintained by the LWG could be improved may propose a change to the policy in question via the LWG's issue tracker. Generally, modification proposals should solicit discussion and support from members of the Drupal community before being brought forward to the LWG.

Received proposals will be evaluated by the LWG, and may be accepted, rejected, or referred back to the proposer with a request for further elaboration or community discussion on the issue. In some cases, the LWG may refer the issue directly back to the community for further discussion before making a final decision.

#### Appeals

If any of the involved parties feels a decision of the LWG is unreasonable, they can escalate it to an appeal. The appeals person/panel will then review the decision, and may choose to either uphold or change it. In the meantime, the decision of the LWG stands. The appeals panel shall consist of Dries Buytaert and/or his designate(s) for community/social issues, or the Drupal Association for legal issues.

### Membership

The current members of the LWG are:

 - Kevin Reynen (chair) - University of Colorado Boulder
 - Gisle Hannemyr - University of Oslo
 - Shawn DeArmond - University of California Davis

The working group consists of a chair and 4-5 other members. The Drupal Association Board appoints the members of the group.

### Contact

For discussions or proposed amendments to technical-related policies, ideas of initiatives to help improve the health of the Drupal community, or other items that could benefit from wider community discussion, you can use the [Drupal License Working Group](http://drupal.org/project/issues/drupal_lwg) issue tracker. Issues in this queue will be reviewed by the LWG on a periodic basis.

### Charter revisions

The charter will be revised as needed. Any proposed charter revisions must be ratified by Dries Buytaert and/or his designate(s) prior to acceptance into this charter. In the future, this charter may be revised to modify the charter revision process, subject to the aforementioned condition.
