## Project Update Working Group Charter

### Mission

The Project Update Working Group helps maintainers prepare contributed projects for the next major release of Drupal core.

### Vision

To maximize the number of contributed projects that are compatible with the next major release of Drupal core.

### Scope

The Project Update Working Group has the ability to appoint its members as a temporary maintainer of a project. 
The only task of the temporary maintainer is to review, test and commit a patch or merge request that makes the module compatible with the next Drupal major release and optionally create a new release.

### Duties / Process

The Project Update Working Group will be able to take this action to prepare a contributed project for the next major release of Drupal core in the following circumstances:

1) The project MUST have a canonical issue for updating to the next major version of Drupal. This issue MUST have a patch or merge request. The issue MUST be marked "Reviewed & tested by the community" and MUST NOT have had feedback from a module maintainer within the past two weeks. The following proposal refers to this as the _contributed project issue_.
1) An attempt MUST have been made by members of the community to contact the module maintainers via their Drupal.org contact form. Record of this attempt MUST be recorded on the _contributed project issue_.
1) An attempt SHOULD be made by members of the community to contact the module maintainers via a messaging platform such as the Drupal community Slack. Record of this attempt MUST be recorded on the _contributed project issue_.
1) If there is no response from the module maintainer for seven (7) days, a member of the community MAY escalate the module to the Project Update Working Group. To escalate a module, create a separate issue in the [Project Update Working Group issue queue](https://www.drupal.org/project/issues/puwg). This is termed the _project update group issue_. An attempt SHOULD be made to notify members of the Project Update Working Group via a messaging platform such as the Drupal community Slack.
1) The Project Update Working Group MUST make a subsequent attempt to contact the module maintainers via their Drupal.org contact form. This communication MUST outline that failure to respond within seven (7) days may result in the Project Update Working Group committing the _contributed project issue_ on their behalf. Record of this contact MUST be recorded on the _contributed project issue_. Any communication between the Project Update Working Group and the module maintainers MUST be recorded on the _project update group issue_.
1) When the seven-day period from item 5 has elapsed, the maintainer has had two weeks overall to respond. At this point, a member of the Project Update Working Group MUST decide on the next step. The next step is to either intervene or not. If the decision is to intervene, then the group must also decide if a tagged release is to be made as well as committing the change.  When making the decision the Project Update Working Group member MUST do the following.
    1) Take into consideration recent activity from the maintainer in the project.
    1) Take into consideration the age of the _contributed project issue_.
    1) Take into account the complexity of the patch/merge request. They must work to avoid regressions. The level of automated test coverage for the project SHOULD be used to inform the likelihood of a regression.
    1) Take into account the quality of the reviews.
    1) Take into account the possible lifespan of the module and the needs of the community. For example, if the module duplicates functionality added to core or another module, then they may decide not to intervene.
    1) Consider if the module is looking for new maintainers and if anyone has nominated themselves for the role. The Project Update Working Group SHOULD favor supporting a new maintainer over intervention.
    1) The Project Update Working Group SHOULD aim to achieve compatibility with the major version in a backwards-compatible way.
1) If a member of the Project Update Working Group decides to intervene and commit the patch, then the following occurs:
    1) A record of the decision MUST be recorded on the _contributed project issue_.
    1) The member of the Project Update Working Group MUST nominate to make the commit and/or release. Record of this nomination MUST occur on the _contributed project issue_.
    1) The member of the Project Update Working Group MUST make a temporary change to the project's maintainers to add themself as a maintainer. Record of this change MUST be made on the _contributed project issue_.
    1) The member of the Project Update Working Group with temporary maintainer access will then commit the  patch or merge request. This MUST be recorded on the _contributed project issue_. 
    1) The member of the Project Update Working Group MUST acknowledge that the commit was made on the _contributed project issue_.
    1) If it was decided that a release should be made, a member of the Project Update Working Group will create a tag and add a release node for the tag on Drupal.org. The member making this action MUST make a record of this on the _contributed project issue_. The release MUST follow semantic versioning rules for backwards compatibility. The member SHOULD strive to make a new minor version to allow sites to install a compatible version without updating the major version of Drupal.
    1) If the module maintainer did not request assistance from The Project Update group, a member of the Project Update Working Group MUST update the project node on Drupal.org to change it to 'No further development'. If the module has opted in to Security team coverage, the member of the Project Update group MAY opt the module out of this coverage.
    1) Any member of the Project Update Working Group MUST then mark the original _contributed project issue_ as fixed. This action SHOULD NOT prevent opening of new issues for the project for major version compatibility.
    1) A member of the Project Update Working Group MUST revoke the temporary maintainer rights within fourteen (14 days). Record of this change MUST be recorded on the _contributed project issue_. 
    1) If the module was marked 'No further development' and if no such issue exists for the contributed project, a member of the Project Update Working Group MUST open a new issue in the project's queue seeking a new maintainer.
    1) If additional compatibility issues are found between the module and the next major version of Drupal, the process above repeats.

### Process

Upon its establishment, the Project Update Working Group will decide on standing business and processes such as regular meetings. This charter will be updated to reflect those decisions.

### Transparency and Appeals

The Project Update Working Group aims to be as transparent as possible by documenting its decisions publicly. Individuals who do not agree with a given Project Update Working Group decision may escalate to Dries Buytaert and/or his designate(s), who will review the decision and can choose to either uphold or alter it. In the meantime, the decision of the Project Update Working Group stands.

### Membership

The working group will comprise community members who self-nominate. Interested community members must receive two seconding recommendations from other community members. Nomination and seconding will occur publicly on Drupal.org in the [Project Update Working Group issue queue](https://www.drupal.org/project/issues/puwg). Community members will be able to share their thoughts or concerns on the nominees' applications. Concerns relating to conduct of members of the group MUST follow Drupal's standard [Community Working Group processes](https://www.drupal.org/community/cwg).

The initial membership of the group will comprise at least five (5) individuals. Members of the group should have a record of maintaining core or contributed projects and have the [ability to opt into security-coverage for projects on Drupal.org](https://www.drupal.org/docs/develop/managing-a-drupalorg-theme-module-or-distribution-project/security-coverage/opting-into/apply-for-permission-to-opt-into-security-advisory-coverage#s-where-do-i-see-if-i-can-opt-projects-into-security-advisory-application). In addition, the group may contain provisional members. These members will not have the ability to change project maintainers and will require the support of a full member to carry out their duties.

The initial makeup of the group will be vetted by the Core Committer Team and Security Team. Subsequent appointments will be vetted by the Project Update Working Group with a fourteen-day period for veto from the security team and/or core committers.

Membership of the group is for a single major update. For example, from Drupal 10 to Drupal 11. The first major update in which the group is active will be from Drupal 10 to 11. At the end of each major cycle, members can opt to renew their membership for the next major update cycle. As with the original nomination, this process will happen in public and require two seconding recommendations from the community.

Once established, the membership will be documented on the [Project Update Working Group](https://www.drupal.org/project/puwg) project page.

### Contact

Use the [Project Update Working Group issue queue](https://drupal.org/project/puwg).

### Charter revisions

The charter will be revised as needed. Any proposed charter revisions will be made from the [governance issue queue](https://drupal.org/project/governance).


