## Security Working Group Charter

### Mission

The mission of the Security Working Group (SecWG) is to ensure that Drupal core and Drupal's contributed project ecosystem provide world-class security, and to provide security best practices for site builders and module developers.

The SecWG acts as a group to review objective and subjective measures of the Security Team, maintain processes focused on resolution and communication of security issues in an efficient manner, and ensure the Security Team has the resources (both technical and membership-wise) and processes to work well.

### Scope / Duties / Responsibilities

#### Scope

The SecWG exists to ensure that the Security Team is functioning well. As such it considers metrics of issues on security.drupal.org (s.d.o), surveys of the team, and surveys of the community. It maintains policies relating to security within the Drupal ecosystem.

#### Specific Duties of the SecWG

- Review and enact changes to processes to ensure they are working well for all stakeholders.
- Ensure the documentation of the Security Team processes is updated and accurate.
- Be an authoritative resource for concrete decision-making around changes or additions to security team policies.
- Periodically create surveys of the Security Team and community members. Review the results of the surveys as a SecWG and then with the security team.
- Determine which metrics are important and review metrics relating to Drupal’s security health.
- Develop guidelines and/or recommendations regarding the introduction of new tools, technologies, and processes for the benefit of security (including security.drupal.org). Where implementation of these recommendations is dependent on Drupal Association funding, Drupal.org integration, or significant investment from the infrastructure team, the SecWG will work with these groups to evaluate feasibility of any particular option or recommendation.

### Specific Duties of the Security Team

While the SecWG owns the policies and processes that the Security Team uses, the definition of those policies and processes are expected to be developed within broad consensus from the Security Team members.

- The Security Team duties are defined here: https://drupal.org/security-team

### Exclusions

- Neither the SecWG nor the security team responds to specific requests for advice on security issues.
- The SecWG has no responsibility over Drupal.org security.
- The SecWG cannot change or extend its own charter.

### Process

The SecWG meets quarterly or more often as needed to do its work. In general, they will propose an idea and ask for objections and set a deadline for when they will take action (minimum two weeks).

### Transparency and Appeals

The SecWG aims to be as transparent as possible by documenting its decisions publicly. Individuals who do not agree with a given SecWG decision may escalate to Dries Buytaert and/or his designate(s), who will review the decision and can choose to either uphold or alter it. In the meantime, the decision of the SecWG stands.

### Membership (in no particular order)

The current members of the SecWG are:

 - Michael Hess / mlhess
 - Greg Knaddison/ greggles
 - Ben Jeavons / coltrane

SecWG members are selected from the Drupal community by Dries Buytaert and/or his designate(s). The SecWG consists of the Security Team Lead, plus 2-5 members of the Security Team, to ensure that the decisions made within the SecWG are in harmony with the people contributing to security.

### Contact

Primary issue queue for the SecWG is (https://drupal.org/project/securitydrupalorg). Use "Security Working Group" tag to bring specific issue to the attention of SecWG.

### Charter revisions

The charter will be revised as needed. Any proposed charter revisions must be ratified by Dries Buytaert and/or his designate(s) prior to acceptance into this charter. In the future, this charter may be revised to modify the charter revision process, subject to the aforementioned condition.
