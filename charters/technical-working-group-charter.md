## Technical Working Group Charter

### Mission

The mission of the Technical Working Group (TWG) is to make sure that the Drupal project has efficient technical policies, procedures, and standards as required to keep the "technical" side of our community operating smoothly.

### Scope / duties / responsibilities

#### Scope

The TWG's job to ensure that the necessary policies, procedures, and standards exist to address technical concerns affecting the Drupal community. This includes items that affect all projects, such as the Drupal coding standards, Git repository usage and licensing policies, and Drupal’s full-project approval process.

The TWG does not necessarily author and maintain these policies itself, but merely ensures that they exist, that they are well maintained, and that they are successful. To do so, they appoint and empower individuals or groups within the community to care for these policies and review them as needed.

The TWG may also develop guidelines and/or recommendations regarding the introduction of new tools and technologies for the benefit of the community. Where implementation of these recommendations is dependent on Drupal Association funding, Drupal.org integration, or significant investment from the infrastructure team, the TWG will work with these groups to evaluate feasibility of any particular option or recommendation.

Acceptance of TWG recommendations for key strategic issues may be subject to approval by the project lead (Dries Buytaert) or his designate(s).

#### Specific Duties of the TWG

- Ensure the Drupal project has effective policies, procedures, best practices, and standards to guide technical aspects within the Drupal community. The TWG does not set policies or standards for individual projects, including core.
- Provide clarification regarding intended interpretation of technical policies, as needed, to assist with conflicts or disagreements related to the interpretation of a technical policy, procedure, or standard maintained by the TWG.
- Ensure that changes to technical standards and policies are published and communicated out to the wider Drupal community.
- Provide recommendations to the Drupal Association, Drupal.org working groups, and infrastructure teams regarding proposed tool changes that will help accelerate contribution or otherwise enable the community.
- Establish best practices and recommendations regarding project namespace allocation, commit access to the Drupal.org Git repositories, the bug/issue/patch/review process, abandoned module process, and other key community workflows.
- Maintain a curated list of standards and policies maintained by the group, and propose additional standards and policies as the need for such is identified. Expansion of the TWG’s scope to include these proposed additions shall be subject to approval by Dries Buytaert and/or his designate(s).

#### Exclusions

Items specifically *not* within the scope of the TWG’s charter:

- The TWG does not get involved with direct personal conflicts, outside of advising on and enforcing existing technical policies, unless mediation assistance is requested by the Community Working Group (CWG).
- The TWG does not direct day-to-day implementation or decisions regarding the operation of Drupal.org or the supporting infrastructure; though TWG supported recommendations and policies may indirectly influence longer-term strategic operational or infrastructure decisions.
- The TWG does not determine which patches or features to commit to any project, including Drupal Core. This responsibility belongs to the individual project and branch maintainers.
- The TWG’s focus is on enablement and support of the Drupal community, not the core Drupal project. The TWG does not drive the technical roadmap or strategic decisions for Drupal core or contrib, including (but not limited to) policies on backwards compatibility or release timing and management; excepting those which have been sanctioned as falling within the TWG’s scope of responsibility (e.g. maintaining the Drupal Core coding standards).
- The TWG may not enforce a newly created standard or policy until inclusion of that policy into the TWG’s scope of responsibility has been officially approved and sanctioned by Dries Buytaert and/or his designate(s).

### Process

#### Requesting technical clarification

Individuals seeking clarification on any policy, procedure, best practice, or standard maintained by the technical working group should first refer to the official TWG documentation regarding the policy or standard in question. If still unclear, and/or the documentation itself could benefit from further elaboration, individuals should bring their concern forward to the TWG. When seeking clarification from the TWG, be sure to provide enough background information for the TWG to understand the context in which the question is being asked.

#### Proposing changes to existing policies and/or standards

Any party who feels a policy or standard maintained by the TWG is unreasonable may propose a change to the policy in question. Generally, modification proposals should solicit discussion and support from members of the Drupal community before being brought forward to the TWG.

Received proposals will be evaluated by the TWG, and may be accepted, rejected, or referred back to the proposer with a request for further elaboration or community discussion on the issue. In some cases, the TWG may refer the issue directly back to the community for further discussion before making a final decision.

#### Resolving technical conflict

Where disagreement should arise due to varied interpretations of a particular policy or standard maintained by the TWG, individuals may request clarification from the TWG as identified above. If the TWG’s clarification is not sufficient to resolve the disagreement, individuals should step through the Conflict Resolution Process (CRP), with the assistance of the CWG if necessary. If further involvement from the TWG is required to assist in resolving the conflict (for example, the disagreement can only be resolved through an addition or modification to an existing policy), the CWG may then refer the matter back to the TWG for resolution.

#### Transparency and Appeals

The TWG aims to be as transparent as possible by documenting its decisions publicly.

Individuals who do not agree with a given TWG decision may escalate to Dries Buytaert and/or his designate(s), who will review the decision and can choose to either uphold or alter it. In the meantime, the decision of the TWG stands.

### Membership

The current members are (in alphabetical order):

- Dave Long (longwave)
- Lee Rowlands (larowlan)
- Nathaniel Catchpole (catch)
- Victoria Spagnolo (quietone)

Members are appointed by Dries Buytaert and/or his designate(s).

### Contact

For discussions or proposed amendments to technical-related policies, ideas of initiatives to help improve the health of the Drupal community, or other items that could benefit from wider community discussion, you can use the [Drupal Technical Working Group issue tracker](http://drupal.org/project/issues/drupal_twg). Issues in this queue will be reviewed by the TWG on a periodic basis.

### Charter revisions

The charter will be revised as needed. Any proposed charter revisions must be ratified by the Drupal Association Board and/or their designate(s) prior to acceptance into this charter.
