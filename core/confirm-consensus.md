# Method used to confirm consensus

To ensure core policy decisions are agreed to by all committers and not just those participating on the issue the following method is used. This method does not need to be used if all the committers are meeting in person.

## Slack

1. When a core policy issue on Drupal.org reaches consensus, the Issue Summary is updated with the proposal and the issue is set to "Reviewed & tested by the community" (RTBC).
1. In committer Slack, the issue is announced in the dedicated 'decisions' channel for confirmation.
1. The announcement starts a 14-day confirmation period. The person posting the announcement should set a Slack reminder for the end of the confirmation period.
1. During the confirmation period;
   1. Any committer may request an extension of the confirmation period. This allows for people who are not available during the confirmation window. As well as if anyone wants to think more about the issue or have an as-yet-undocumented objection. In this case, it's up to that person to follow up when they're available. If they do not respond after the extension, the timer is restarted.
   1. non-blocking changes such as wording changes or other small changes that align with the already built consensus can be made. The Issue Summary is updated accordingly.
   1. If a blocking comment is made, the issue is set to "Needs review" or "Needs work" as needed.
1. If there is explicit, unanimous support for the issue, the confirmation period is terminated, and the issue is set to "Fixed".
1. After the confirmation period, if there is nothing blocking the proposal, the issue is set to "Fixed".
