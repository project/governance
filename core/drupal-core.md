## Introduction

This document provides an overview of Drupal core's organizational structure, outlines what roles/responsibilities exist, and defines how decisions are made. The goal is to help the broader community understand the process, to ensure that both old and new maintainers understand their responsibilities, and to eliminate uncertainty about who has the ability to make decisions in various parts of the project.

This is a living document; if you see something out of date or missing, please file an issue in the ["Drupal core" issue queue](https://www.drupal.org/project/issues/drupal) with the "[Project governance](https://www.drupal.org/project/issues/search/drupal?issue_tags=Project+governance)" issue tag.

For Drupal 8 and above, the contributors in each role are listed in the most recent version of [core/MAINTAINERS.txt](https://git.drupalcode.org/project/drupal/raw/HEAD/core/MAINTAINERS.txt). For Drupal 7, they are listed in the most recent version of [MAINTAINERS.txt](https://git.drupalcode.org/project/drupal/-/blob/7.x/MAINTAINERS.txt). That document uses 'Branch maintainers' to refer to 'Core committers' and 'Component maintainers' for 'Subsystem maintainers'.

## What is a core contributor? ([\#](#contributor-definition))

Anyone at all with a Drupal.org account can be a core contributor, simply by participating in the Drupal core issue queue. A core contributor is someone who:

- Posts bug reports and feature requests
- Uploads code changes ("patches") to the issue queue
- Reviews the behavior and code changes from a given patch
- Posts constructive, actionable feedback on how other core contributors can improve their work
- Performs "triage" of the issue queue, such as closing out duplicate issues, setting metadata properly
- Contributes their skills and experience such as architecture, design, accessibility, access to non-technical users, and development
- Various other collaborative activities

In short, core contributors are empowered to drive changes within Drupal's code base, and to work with other core contributors to help get changes they care about in, using a variety of both technical and non-technical skills.

Most proposed changes follow a standard peer review process, with sign-off from one or more maintainers (who are themselves a subset of core contributors).

## What is a maintainer? ([\#](#maintainer-definition))

(Thanks to [Docker](https://github.com/docker/docker/blob/master/MAINTAINERS) for the inspiration.)

There are different types of maintainers, with different responsibilities, but all maintainers have three things in common:

1. They share responsibility for the project's success.
1. They have made a long-term, recurring time investment to improve the project.
1. They spend that time doing what needs to be done, not always what is the most interesting or fun.

Maintainers are essential, but the results of their work are not always immediate or obvious. It's easy to appreciate a really cool and technically advanced feature. It's harder to appreciate the absence of bugs, the slow but steady improvement in stability, or the reliability of a release process. But those things distinguish a good project from a great one.

Potential maintainers generally demonstrate expertise, consistency, and passion contributing in their area before they become maintainers.

Maintainers with commit access need additional skills. They are reliable, decisive when needed, open to feedback, and able to give others feedback constructively. They also need significant prior experience participating broadly in core issues (usually at least a year).

It's generally desired to have at least two people in any given maintainership role, in order to prevent burnout and to help ensure there are ample people available for unblocking a given decision; however, this is not a hard requirement. (See [How should maintainers in the same role share responsibilities?](#faq-teams) for more on maintainer teams.) Maintainers are expected to give clear, objective reasons and actionable feedback in the event they push back on a given change.

## What are the different types of maintainers? ([\#](#maintainer-types))

The list below defines the different maintainer roles. ([Detailed responsibilities for each role](#who-are-maintainers) are outlined later.)

- **Project Lead**: The chief decision-maker for the project.
- **Core Committers**: The small subset of people who, along with the Project Lead, can commit changes to Drupal core itself. Within the core committer team there are:
    - **Product Managers**: Focus on user-facing features and Drupal's user experience.
    - **Framework Managers**: Focus on cohesiveness of Drupal's architecture, APIs, and developer experience.
      - **Backend Framework Managers**: are collectively responsible for PHP, bash, and database APIs.
      - **Frontend Framework Managers**: are collectively responsible for Twig, HTML, CSS, and JavaScript APIs.
    - **Release Managers**: Focus on processes to increase release stability, and on facilitating effective collaboration among contributors.
- **Subsystem Maintainers**: Maintainers of one particular part of Drupal core, such as the Entity system or Comment module.
- **Topic Maintainer**: Subject-matter experts in a topic that spans multiple subsystems—Accessibility, Documentation, Performance, Testing, and Usability. These correspond to the [core gates](https://www.drupal.org/core-gates) which all proposed changes must pass.
- **Initiative Coordinator**: Community members appointed by the Project Lead to coordinate strategically important work. Initiatives are typically cross-functional in that they span different subsystems and different topics, and can affect both Drupal the product and Drupal the framework.

## How are decisions made? ([\#](#decisions))

Anyone with a Drupal.org account can create or update a Drupal core issue to propose a change. Issues should be processed according to the following flow:

1. Drupal employs a [peer review process](https://www.drupal.org/patch/review). All proposed changes require [sign-off](#faq-sign-off) from someone other than the author(s).
1. The [core committers](#committer) use consensus decision-making.
1. If a change doesn't [*significantly*](#faq-significant) impact any given area of Drupal, it simply needs a peer's sign-off.
1. If a patch [*significantly*](#faq-significant) impacts a single subsystem, the [maintainers of that subsystem must be ](#subsystem)[given opportunity to sign off](#faq-timeframe).
1. If the change [*significantly*](#faq-significant) impacts any area covered by a [topic maintainer](#topic), that topic maintainer needs to approve or refuse it.
1. If the change is adding additional maintainers, other maintainers for that area must be [given opportunity to sign off](#faq-timeframe).
1. If a subsystem/topic maintainer sign-off is not given in a [timely manner](#faq-timeframe), or if sign-off is not possible because the patch was authored by those who need to provide sign-off, it gets escalated to the core committers.
1. If the change significantly impacts *multiple* subsystems, or represents a [*significant*](#faq-significant) change in architecture or public APIs, the [framework managers](#framework-manager) must approve or refuse it.
1. If the change represents a [*significant*](#faq-significant) new feature, UI change, or change to the "user experience" of the project, the [product managers](#product-manager) must approve or refuse it.
1. If the change [*significantly*](#faq-significant) affects the overall technical debt or release timeline of the project, the [release managers](#release-manager) must approve or refuse it.
1. Any [core committer](#committer) may commit a change to remove a maintainer at that maintainer's own request.
1. If the change involves adding a new subsystem maintainer, a [product](#product), [framework](#framework), or [release](#release) manager must approve or refuse it (depending on the subsystem).
1. If the change is adding new full or provisional [core committers](#committer), [subsystem maintainers](#subsystem) or [topic maintainers](#topic), then it must be approved by the [core committer team](#core-committers). The same is true if there is any cause for concern with adding a maintainer, or if the change involves removing a maintainer except at their specific request.
1. If the change is adding new full or provisional [initiative coordinators](#initiative), the [Project Lead](#project-lead) must approve or refuse it. The same is true if there is any cause for concern with adding a maintainer, or if the change involves removing a maintainer except at their specific request.
1. If the change affects the governance, philosophy, goals, principles, or nature of the project, it must be approved by the [Project Lead](#project-lead).
1. Once a change has the required sign-offs, any [core committer](#committer) may [commit](#faq-patches-committed) it.

See the [Frequently Asked Questions](#faq) for explanations of what it means to sign off on a change and examples of what constitutes a "significant" change.

Also see the [Responsibility Assignment Matrix version](#appendix-responsibility-assignment).

## Who are the Drupal core maintainers, and what do they do? ([\#](#who-are-maintainers))

This section:

- Defines in detail each of the roles discussed above.
- Outlines their overall responsibilities and the tasks they're expected to do.
- Lists both the current and ["provisional"](#faq-provisional) membership of each role.

### The Project Lead ([\#](#project-lead))

All decisions are made, by default, by the Project Lead. However, since the Project Lead making every decision would be highly un-scalable, in practice decisions are spread across multiple maintainers. The Project Lead appoints and replaces those maintainers, and spends time thinking about how to make the team function well. The Project Lead also holds final say in any disagreements, and can be escalated to when decisions cannot be reached through the normal process. Finally, the Project Lead also preserves the philosophy, culture, and principles of the project.

#### Tasks

- Are there issues assigned to the Project Lead which have been escalated by the other maintainers?
    - If yes: either resolve them, or appoint someone to resolve them
- Are there "Project governance" issues that need review?
    - If yes: appoint new maintainers, and/or replace maintainers.
- Is the project governance stuck in a deadlock, irreversibly fragmented, or off track?
    - If yes: refactor the project governance.

### Core Committers ([\#](#committer))

Core committers are the people in the project with commit access to Drupal core. Within the core committer team, the Project Lead assigns people certain areas of focus (defined below) to help with decision-making. Nevertheless, core committers are a group of peers, and are expected to work closely together to achieve alignment, to consult each other freely when making difficult decisions, and to bring in the Project Lead as needed.

While core committers tend to stay focused on their particular versions, where needed they can step in to fill needs in other versions (for example, the Drupal 7 Release Manager rolling a security release for Drupal 6 at the same time). It's also possible that a non-technical Product Manager or Release Manager could be appointed at some point, who would only very rarely use their commit access. The distribution of committing work is something to be worked out among the core committer team.

#### Tasks

- Are there issues in the "Reviewed &amp; tested by community" (RTBC) issue queue?
    - If yes: Verify the issues have proper sign-offs and review the changes therein; commit the change or modify issue statuses/assignments as appropriate.
- Was a change escalated to your area of focus?
    - If yes: Either make a decision or ask advice from one of the similarly focused committers.
- Does a change span multiple areas of core committer concern (product, framework, and, release management?)
    - If yes: A relevant subset of committers meets as a group to discuss.
    - Is there consensus among affected core committers on what to do?
        - If yes: Post a summary to issue.
        - If no: Escalate to the Project Lead.

### Core Committer: Product Manager ([\#](#product-manager))

Product managers are expected to talk extensively with different types of Drupal’s users, gather data about how Drupal is used in the “real world,” and present this information back to Drupal's various audiences.

The product managers are responsible for:

- Determining what user-visible features Drupal core ships with
- Drupal's out-of-the-box experience
- The overall integrity of Drupal’s user experience
- Representing the Drupal product's audiences (site builders, content authors, etc.) in core decisions

Significant changes to user-visible features, the user interface, and install profiles must be approved by the product managers.

#### Tasks

- Are there issues tagged "Needs product manager review"?
    - If yes: provide feedback and make a decision.

### Core Committer: Framework Manager ([\#](#framework-manager))

The framework managers are responsible for the overall integrity of the technical architecture across all subsystems, the consistency of APIs, and the overall developer experience of Drupal.

Significant developer features, changes to public APIs, external libraries, and overall architecture must be approved by the framework managers.

#### Tasks

- Are there issues tagged "Needs framework manager review"?
    - If yes: Provide feedback and make a decision.

### Core Committers: Release Manager ([\#](#release-manager))

The release schedule is set by the Project Lead in consultation with the other core committers, and the release managers make sure that we manage towards it. This includes rolling the releases, tracking the release schedule, facilitating communication between all participants, measuring progress, and defining the processes that help contributors collaborate effectively and efficiently. Release managers also make the final call on issue "metadata", such as status and priority, unless overruled by the Project Lead.

#### Tasks

- Are there issues tagged "Needs release manager review"?
    - If yes: Provide feedback and make a decision.
- Has the next release been scheduled?
    - If no: Schedule it; if it's scheduled for today, tag it. Coordinate with the security team as needed.
- Are all release blockers and risks properly identified,triaged, and on a path to resolution?
    - If no: Coordinate triage of critical issues or propose steps to accelerate them.

### Committer team facilitator ([\#](#commiter-team-facilitator))

The committer team facilitator is responsible for meetings within the team, with the Drupal Association, and with the community. They make arrangements for meetings, and keep formal records of the discussions and decisions. They also facilitate and communicate core process and decisions.

#### Tasks
- Ensure committer team meetings are scheduled regularly.
- Organize meetings at major Drupal events between core committers and core stakeholders.
- Prepare core meeting agendas and contribute to the agendas of meetings with the Drupal Association.
- Facilitate discussion in meetings.
- Identify action items and owners for each.
- Ensure action items from past meetings are completed.
- Help communicate committer and other maintainer decisions to community members.

### Topic Maintainers ([\#](#topic))

Some aspects of the project are system-wide and correspond to one of the core gates that all proposed changes must pass. Topic maintainers ensure that patches improve rather than cause regressions for these topics and help educate other contributors on how to do the same in their patches.

#### Tasks

- Are there any issues tagged "Needs $topic review"?
    - If yes: Provide review.

#### Length of term

The term of a topic maintainer is not fixed. All topic maintainers are contacted once per year and asked to submit an merge request removing themselves from MAINTAINERS.txt if they are not currently active (i.e., have not at least contributed in the role in the past year). Maintainers that do not respond to this contact are automatically removed from the role. Maintainers who have low activity may be contacted as well on a case-by-case basis.

### Subsystem Maintainers ([\#](#subsystem))

Subsystem maintainers oversee the development of parts of Drupal known as "subsystems". They can make a shorter-term commitment to maintaining a subsystem, or a longer-term commitment that spans several major Drupal versions.

The maintainers of each subsystem are responsible for:

- Communicating the vision for their subsystem.
- Delivering prompt feedback and decisions on issues affecting their subsystem when needed.
- Being available to other contributors with questions, bug reports, and feedback on their component, primarily on Drupal.org issues.
- Working with the security team to resolve security issues in the subsystem.
- Ensuring their subsystem respects the philosophy, design, and goals of the project.
- Periodically triaging their subsystem's issue queue to ensure that issues therein are relevant and current.

Anyone can be a provisional maintainer to a subsystem by simply contributing to it. Most subsystem maintainers start out contributing to their component by triaging issues, supplying patches, and reviewing others' patches.

#### Tasks

- Are there issues tagged "Needs subsystem maintainer review" in your component?
    - If yes: Provide [prompt feedback](#faq-timeframe) and make a decision.
- Are there contributors in your queue who are providing consistently great code and reviews?
    - If yes: File an issue with the "Project governance" tag recommending them as fellow subsystem maintainers.
- Are there fellow maintainers in your subsystem who are no longer active?
    - If yes: Try and reach out to them first.If there is no response, file an issue against the "Project governance" tag recommending removal.

Subsystem maintainers also ensure that the issues in their queues are properly triaged, either through their own work or by organizing other contributors:

- Are there issues you've not yet responded to in your subsystem's issue component?
    - If yes: Provide review and feedback.
- Are there duplicate issues in your subsystem, or issues that more rightly belong in another subsystem?
    - If yes: Triage issue queue accordingly.
- Do some of the issues in your subsystem represent significant changes to user experience, architecture, release timelines, or one of the topics?
    - If yes: Tag "Needs X review" accordingly.

#### Length of term

The term of a subsystem maintainer is not fixed. All subsystem maintainers are contacted once per year and asked to submit a merge request removing themselves from MAINTAINERS.txt if they are not currently active (i.e., have not at least contributed in the role in the past year). Maintainers that do not respond to this contact are automatically removed from the role. Maintainers who have low activity may be contacted as well on a case-by-case basis.

### Initiative Coordinator ([\#](#initiative))

Initiatives are larger projects for the current development release that are intended to re-architect or otherwise improve large areas of the Drupal core code-base. The Project Lead determines these initiatives. Each initiative has initiative coordinators who act as project and community managers with a strategic agenda, and help guide a team of people interested in helping the initiative to succeed. Initiative coordinators don't have direct decision making powers; rather, they work to build buy-in among those who do have decision-making powers (product managers, subsystem maintainers, topic coordinators, etc.).

#### Tasks

- Coordinate work on the initiative.
- Communicate the plans, progress, and needs of the initiative to the community and the other maintainers.

### Core initiative facilitator ([\#](#core-initiative-facilitator))

Initiative facilitators are members of the committer team who help with multiple initiatives and assist individual [initiative coordinators](#initiative) to work effectively with the core team and other initiatives.

#### Tasks

- Surface issues from initiative contributors to other initiatives and the committer team.
- Ensure that initiative teams are adequately aware of release cycle requirements and deadlines.
- Ensure that initiative feature roadmaps are accurate and have the signoff of product managers.
- Spread best practices within initiatives, and mentor initiative leads.
- Help communicate initiative needs and successes publicly.
- Plan strategic core "birds of a feather" sessions at major Drupal events.

## Frequently Asked Questions ([\#](#faq))

### How is a "significant" change defined? ([\#](#faq-significant))

Here are some examples:

- **A typo fix in the documentation of the Batch API.** This will not significantly impact the Batch API, nor will it significantly impact the topic of Documentation, so only a simple [peer review and sign-off](https://www.drupal.org/patch/review) is needed.
- **A change in Entity API to the arguments of a method that's called from numerous core modules.** The change will be a significant one to Entity API because it has impact on multiple modules. However, to each consuming module it will be a simple one-line fix and therefore insignificant to them. Only the Entity API subsystem [maintainers' sign-off](#faq-sign-off) is needed.
- **The introduction of a brand new configuration system API, which multiple subsystems are expected to use in a consistent way.** In this case, current Configuration subsystem maintainer sign-off alone is not enough; because of the impact to multiple other subsystems, the framework managers must also approve the change (and will likely consult with multiple other subsystem maintainers who would be major consumers of that API in the process). Additionally, the Documentation topic maintainer will need to provide sign-off to ensure the documentation for this new system is clear and complete.
- **Switching the order of fields on a configuration form.** Unless done on a very frequently used page, chances are this would not qualify as a significant change, and simple peer-review sign-off would be sufficient.
- **Introduction of a new UI pattern, such as a new tag selection widget.** Depending on how widespread its use will be, this may require product manager sign-off, but will certainly require both Usability and Accessibility topic maintainer sign-offs to ensure product cohesiveness is retained.
- **Redesign of the node creation form.** Because this change will be to the most frequently-accessed screen for content editors, as well as one of the most frequent targets for module developers for enhancements, it has significant impact on multiple fronts. In addition to the Node module subsystem maintainer, product managers must approve this change (to ensure the quality of the default user experience of Drupal), as must Framework Managers (to ensure that contrib extensibility is retained), as must Usability and Accessibility topic maintainers (to ensure there are no regressions in those areas).
- **Convert the code base from procedural to object-oriented code.** While clearly something that will affect multiple subsystems and thus will fall to framework managers to sign off, this also will have impacts for the very nature of Drupal itself and its target audience, so Project Lead sign-off is also required.
- **A new release process is proposed to define what changes are/are not allowed during beta.** This is squarely in the Release Managers' purview, but they should consult with the rest of the core committers (as well as other core maintainers if they so choose) when formulating a proposal. Any new release process will ultimately need to be signed off by the Project Lead as well, since it will impact both the goals and governance of the project.

### As a maintainer, how do I "sign off" on a change in an area I am responsible for? ([\#](#faq-sign-off))

1. Look for issues tagged ["Needs $role review"](#faq-tags) for your role.
1. Review each issue and provide feedback by leaving a comment.
1. If the issue needs more work, set it to "Needs work" (NW).
1. If you agree with the change, update the issue's summary to indicate that you have signed off on the change.
1. If a committer set the issue back from "Reviewed and tested by the community" (RTBC) for your signoff and no other sign-offs are needed, set it back to RTBC.
1. Remove the "Needs $role review" tag for your role.
1. If you are a committer and no other sign-offs are needed, you can review and commit the change as appropriate.

### Are there objective criteria for sign-offs or is it purely up to the maintainer? ([\#](#faq-criteria))

Drupal core has existing objective criteria for what changes can be accepted, including:

- The [core gates](https://www.drupal.org/core-gates) which apply to all core patches.
- Guidelines on which changes can be made based on the release phase, including the [Core backport policy](https://www.drupal.org/node/767608) and the [Drupal 8 beta allowed changes policy](https://www.drupal.org/core/beta-changes).
- General [coding standards](http://drupal.org/coding-standards).
- Other policies / standards documented under [Getting involved in Drupal core](https://www.drupal.org/contribute/core).

Maintainers both evaluate changes in terms of these criteria and provide specific guidance for their area based on their domain-specific expertise and overall plan. (Also see the overall [Drupal mission, values, and principles](https://www.drupal.org/mission).)

### What happens if sign-offs are missing? ([\#](#faq-sign-off-missing))

Any contributor can suggest that a change needs a particular sign-off and add the appropriate ["Needs $role review"](#faq-tags) tag. (Refer to the [Responsibility Assignment Matrix](#responsibility-assignment) for guidelines.)

Core committers are ultimately responsible for assessing which sign-offs are needed or missing for a given change in the RTBC queue. If one or more sign-offs are missing, the committer will add the appropriate ["Needs $role review"](#faq-tags) tag(s) and set the issue back to "Needs review" or "Needs work". (Also see [How are patches committed?](#faq-patches-committed))

### What timeframes are given for sign-offs before decisions are escalated? ([\#](#faq-timeframe))

In general, when an issue is [tagged as needing a particular signoff](#faq-tags), it is the responsibility of the maintainers for that role to [provide feedback](#faq-sign-off) within one week. If the review is not done within that timeframe, the issue should be escalated to the next step. For time-sensitive issues (such as critical regressions or issues that are blocking other work), this timeframe may be shortened or a committer may recommend a follow-up discussion instead.

Maintainers can and should take time off, whether for vacations or just because they feel the need to. However, when doing so, maintainers should let their co-maintainers and/or the core committers know about their impending absence and what they'd ideally like done with any issues that need their feedback in the meantime, such as delegating these decisions to another individual.

Also see [I disagree with a patch that was committed, and it introduced problems with an area of core I am responsible for. What should I do?](#faq-request-followup) and [How are patches committed?](#faq-patches-committed).

### What are the issue tags used to alert maintainers that a signoff is needed? ([\#](#faq-tags))

- Needs subsystem maintainer review (within the particular subsystem's issue queue)
- Needs usability review
- Needs accessibility review
- Needs performance review
- Needs documentation review
- Needs testing review
- Needs product manager review
- Needs framework manager review
- Needs frontend framework manager review
- Needs release manager review
- Project governance

(There is no "Needs Project Lead review" tag; issues should be tagged with "Project governance" if appropriate or otherwise simply be assigned to the Project Lead by a maintainer.)

### How is commit access allocated? ([\#](#faq-commit-access))

The roles of Project Lead, product manager, framework manager, and release manager have commit access and are collectively referred to as "core committers" (or "branch maintainers" for a specific major Drupal version). Topic maintainers and subsystem maintainers do not have commit access.

### How are patches committed? ([\#](#faq-patches-committed))

Core committers work together to provide [timely feedback](#faq-timeframe) on all issues that are marked "Reviewed &amp; tested by the community" (RTBC), either reviewing and committing patches or setting an issue back to "Needs work" (NW) or "Needs review" (NR) for additional work or (in the case of significant changes) for missing signoffs.

For the current development branch of Drupal, committers work together to provide feedback on issues in the RTBC queue as soon as their availability permits. For stable branches, committers may leave issues at RTBC for a longer period to ensure they are adequately reviewed by others.

Committers will sometimes assign an issue to a different committer who has more expertise for that issue or whose signoff is needed.

### What is a "provisional" maintainer? ([\#](#faq-provisional))

A "provisional" maintainer is a maintainer who does not yet have full authority for their role, but is working alongside the primary maintainers with the aim to become a full-fledged maintainer in the future.

### How should maintainers in the same role share responsibilities? ([\#](#faq-teams))

Multiple maintainers may share the same role; in fact, it often works best to have maintainer teams, as described in the [definition of a maintainer](#maintainer-definition). When there is more than one maintainer in a role, the maintainers collaborate to accomplish the tasks for the role. The maintainers may decide among themselves however they wish to share these responsibilities. They might share the tasks equally, or agree that a certain maintainer within the team will be responsible for a particular task, period of time, part of the subsystem, etc.

If the maintainers in a given role disagree on a decision in their area, the decision may be escalated to the next level.

### Are maintainers required to have any particular skills/expertise? ([\#](#faq-expertise))

No. Maintainers can rely on others from the team to support them in areas where they have less expertise. For this reason, it is valuable to have a range of technical skills on the team, but there are no specific requirements like "at least one of the product managers must have a design/UX background" or "one of the architects must be a performance or front-end engineer".

### Are there any provisions for various maintainers like "no more than X people from a certain company"? ([\#](#faq-provisions))

No. In general, maintainers consult others for any significant decision; see [How are decisions made?](#decisions) above. Since decisions are made through a system of signoffs and consensus rather than by vote, this mitigates the effect of any one group having a larger majority. If there are concerns about a maintainer making biased decisions on behalf of a particular employer or audience, these can be escalated "up the chain" to the next level, up to the Project Lead, or in a worst case scenario up to the [Community Working Group](https://www.drupal.org/governance/community-working-group). (If there are simply objective concerns about a specific decision, a [followup issue](#faq-request-followup) should be filed instead.)

### What's the difference between a "Product manager" and a "Usability topic maintainer"? ([\#](#faq-product))

A product manager sets forth the "vision" for what Drupal will look like, and decides on its feature set. A Usability maintainer helps maintain that vision by enforcing usability guidelines and providing usability feedback in their patch reviews. It's expected that the product managers and usability maintainers will work closely with one another, by drawing on each others' expertise.

### What's the proper way to add myself as a maintainer? What's the proper way to step down as a maintainer? ([\#](#faq-step-up-down))

In either case, file an issue in the ["Drupal core" issue queue](https://www.drupal.org/project/issues/drupal) with the "Project governance" issue tag. If stepping up, it's helpful to list a few issues you've helped with recently to demonstrate your track record. If stepping down, it's helpful to recommend another person to take your place (when possible).

### I disagree with a patch that was committed, and it introduced problems with an area of core I am responsible for. What should I do? ([\#](#faq-request-follow-up))

Cross-post a followup issue explaining your concern and reach out to the core committer. For urgent or critical regressions, reopen the original issue and request a revert.

### Doesn't this structure add all kinds of overhead to the committing process? ([\#](#faq-overhead))

While it's possible that this process may introduce slightly more overhead, the intent is to simply formalize what is informally done anyway, and help make the decision-making process explicit so both contributors and maintainers know what to expect and who to talk to if they're stuck. Also, formally spreading sign-off responsibilities to the various parties will hopefully result in fewer RTBC patches being rejected by core committers and a faster and more efficient process for everyone overall.

## Appendices ([\#](#appendices))

#### Appendix: Responsibility assignment matrix ([\#](#responsibility-assignment))

The following [Responsibility Assignment Matrix](http://en.wikipedia.org/wiki/Responsibility_assignment_matrix) illustrates the Drupal core [decision-making process](#decisions).

While the most common format for such matrices is [RACI](http://en.wikipedia.org/wiki/Responsibility_assignment_matrix#Key_responsibility_roles) (Responsible, Accountable, Consulted, and Informed), this table uses a variation called [PACSI](http://en.wikipedia.org/wiki/Responsibility_assignment_matrix#PACSI) (Perform, Accountable, Control, Suggest, Informed), which more closely matches the collaborative nature of our culture.

##### Key

| Icon | Name        | Meaning                                                                                                                                                                                                                               |
|:----:|-------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 🄿    | Perform     | The role(s) that carry out the activity. This is placed in the column of the role(s) that *predominantly* drive those changes, but this doesn't preclude other roles from also carrying out work.                                     |
| **🄰**    | Accountable | The role(s) ultimately accountable for the correct and thorough completion of the task, and often the ones who delegate the work to the performer.                                                                                    |
| **🄲**    | Control     | The role(s) that review the result of the activity (other than the Accountable). They have a right of veto and their advice is binding.                                                                                               |
| 🅂    | Suggest     | The role(s) consulted for advice based on their expertise. They provide non-binding advice. These are role(s) whose input via two-way communication is actively sought, though this does not preclude others from making suggestions. |
| 🄸    | Informed    | The role(s) that must be informed of the result of the activity.                                                                                                                                                                      |

##### Matrix

Note that if a change includes multiple rows in this table, there will be multiple roles involved.

| Changes that significantly impact...                               | Project Lead | Any Core Committer | Product Manager | Framework Manager | Release Manager | Core Subsystem Maintainer | Topic Maintainer | Initiative Coordinator | Core Contributor |
|--------------------------------------------------------------------|:------------:|:------------------:|:---------------:|:-----------------:|:---------------:|:-------------------------:|:----------------:|:----------------------:|:----------------:|
| (No significant impact in any of the below areas)                  | ✱            | **🄲**                  |                 |                   |                 |                           |                  |                        | **🄰**/🄿              |
| ... A single subsytem                                              | ✱            | **🄲**                  |                 |                   |                 | **🄲**/**🄰**                       |                  |                        | 🄿                |
| ... Multiple subsystems                                            | ✱            |                    |                 | **🄲**/**🄰**               |                 | 🅂                         |                  |                        | 🄿                |
| ... A topic area                                                   | ✱            | **🄲**                  |                 |                   |                 |                           | **🄲**/**🄰**              |                        | 🄿                |
| ... An official initiative                                         | 🄸✱           | **🄲**                  |                 |                   |                 |                           |                  | **🄰**                      | 🄿                |
| ... User-facing core features                                      | 🄸✱           |                    | **🄲**/**🄰**             | 🅂                 | 🅂               |                           |                  |                        | 🄿                |
| ... Developer-facing core features                                 | 🄸✱           |                    | 🅂               | **🄲**/**🄰**               | 🅂               |                           |                  |                        | 🄿                |
| ... Release management procedures                                  | 🄸✱           |                    | 🅂               | 🅂                 | **🄲**/**🄰**/🄿           |                           |                  |                        | 🄸                |
| ... Core release policies or timeline                              | **🄲**            |                    | 🅂               | 🅂                 | **🄰**/🄿             |                           |                  |                        | 🄸                |
| _Any_ impact on principles, goals, or nature of the Drupal project | **🄲**/**🄰**          |                    | 🅂               | 🅂                 | 🅂               |                           |                  |                        | 🄿                |
| _Any_ impact on project governance                                 | **🄲**/**🄰**/🄿        |                    | 🅂               | 🅂                 | 🅂               |                           |                  |                        | 🄸                |

✱ The Project Lead may proactively make or override these decisions if they deem it necessary.
