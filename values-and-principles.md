> The Drupal Values and Principles describe the culture and behaviors expected of members of the Drupal community to uphold. These principles inform how technical and non-technical decisions are made, in addition to how contributors and leaders can support both our community and the project.
> 
> I have strong conviction in each of the Drupal Principles and corresponding Value. The values and principles will allow the Drupal community to make better decisions faster, inspire members to be their best selves and move forward as a unified community.
> 
> Each value and principle is subject to ongoing revision based on experience and feedback. Values and principles are also grouped by topic, and it’s important to note that the order of the principles does not denote hierarchy. The values and principles are maintained by me, Dries Buytaert.
> 
> Dries Buytaert  
> Founder and Project Lead of Drupal

## Values

### Prioritize impact

- [Impact gives us purpose](#impact-gives-purpose). [We build software that is easy, accessible and safe for everyone to use](#build-software-everyone-can-use).

### Better together

- [We foster a learning environment](#foster-a-learning-environment), [prefer collaborative decision-making](#decisions-should-be-collaborative), [encourage others to get involved](#everyone-has-something-to-contribute) and to help[ lead our community](#choose-to-lead).

### Strive for excellence

- We constantly re-evaluate and assume that [change is constant](#change-is-constant).

### Treat each other with dignity and respect

- [We do not tolerate intolerance toward others](#every-person-is-welcome-every-behavior-is-not). [We seek first to understand, then to be understood](#seek-first-to-understand-then-to-be-understood). [We give each other constructive criticism, and are relentlessly optimistic](#be-constructively-honest-and-relentlessly-optimistic).

### Enjoy what you do

- [Be sure to have fun](#be-sure-to-have-fun).

## Principles

### Prioritize impact

#### Principle 1: Impact gives purpose

Drupal has an effect on billions of people around the world, from those who directly contribute to the project to the end users who interact with Drupal-powered experiences every day. Drupal is used in education, communication, collaboration, business, government and more, and it has a tremendous impact on the lives of many.

When contributing to Drupal, the goal is often to optimize the project for personal needs ("scratching our own itch"), but it has to be bigger than that. The goal should also be to maximize the impact the project can have on others. We derive meaning from our contributions when our work creates more value for others than it does for us.

Principle 1 hasn’t always been a priority for Drupal. For example, in the earliest days, the primary focus was to build a message board for friends. When the code behind drop.org was open sourced, the primary focus evolved to building software for developers. As Drupal continued to grow and the technology landscape evolved, that viewpoint began to evolve once more. Our community started hearing more and more inspirational stories about how Drupal had affected people around the globe. Slowly, focus shifted from writing the perfect code to growing the project, and amplifying Drupal's impact through better marketing, user experience, and more.

Today, I believe this principle is non-negotiable, as it fuels the Drupal community’s collective purpose. Prioritizing impact means that every community member acts in the best interest of the project. Optimizing for the greater good informs how difficult decisions or trade-offs are made.

This also informs how project work serves project stakeholders. When faced with trade-offs, prioritize the needs of the people who create content (our largest user base) before the people who build sites (our second largest user base) before the people who develop Drupal (our smallest user base).

#### Principle 2: Build software that everyone can use

The ability to publish, collaborate, and share online is fundamental to how people communicate. Although the internet was previously a vehicle for education and entertainment, it is now ingrained into nearly every aspect of daily life. It’s how people pay bills, stay in touch with friends and family, and even manage their healthcare. It is a workspace, a social space, and the fastest conduit to answers and discovery.

As more transactions, collaborations and interactions are taking place online, there is now a greater responsibility to ensure that the web is inclusive of every person and accounts for everyone’s safety. Drupal has such a large impact on the digital landscape that our community cannot afford to be careless.

When people are excluded from being able to access online experiences, they are also excluded from rewarding careers, independent lifestyles, and the social interactions and friendships that bring people together and foster innovation. When software is unsafe, countless people are put at risk of financial, social, and physical harm.

Our community needs to ensure that everyone has access to Drupal and that everyone can use Drupal easily and safely. Making security, privacy, accessibility, multilingual capability, and usability top priorities is hard work, but it’s worth it. We want to build "good software."

### Better together

#### Principle 3: Foster a learning environment

Commit to establishing and maintaining a transparent environment that enables community members to learn and grow through inquiry and curiosity.

Asking questions or sharing ideas can be difficult, especially if the questions or ideas are not fully formed or if the individual is new to Drupal. This is not exclusive to Drupal, as everyone can remember a situation where they felt vulnerable asking a question.

Being vulnerable and taking risks are essential to the learning process. Therefore, create an environment where people can ask questions safely and share ideas comfortably. Do not tolerate behaviors that demean others' sincere contributions, ideas, or questions.

Long-term contributors should be role models by admitting their own shortcomings and mistakes, being vulnerable, and by giving others the same respect that was once given to them.

Every individual can make a difference when we empower each other. Throughout the history of the Drupal community, many individual contributors have made a significant impact on the project. Helping one person get involved could be game-changing for the Drupal project.

Contributing to Drupal often requires many talents that can be hard to master. When an individual is new to Drupal, it might feel overwhelming. Know that at one point or another, everyone was new to Drupal. We hope every contributor will find the support and guidance they need to contribute.

If you choose to push through the learning process and become a contributor, you will most likely feel an incredible sense of reward and excitement. Through your collaboration with others, you'll push your limits and improve your leadership skills. Best of all, in addition to forging new connections and friendships, you’ll help make a positive impact on millions of people around the globe.

#### Principle 4: Decisions should be collaborative

Different approaches to decision-making exist; decisions can be made unilaterally, collaboratively, or via consensus. Each situation is unique and may require a unique approach. However, we always try to prioritize collaborative decision-making.

Consensus-based decision-making is often a slow process and is not an ideal approach. This method often pushes groups to focus on the various weaknesses of a given idea instead of placing value on the few but important gains. As a result, proposed solutions have no sharp weaknesses but also lack world-class strengths. When everyone is mostly happy, no one is completely happy.

In contrast, collaborative decision-making allows leaders to collect data and learn from others but still gives leaders the agency to make decisions that go against the group's opinions.

The first step in making a collaborative decision, particularly if there is disagreement, is to take time to understand every aspect of the problem you are aiming to solve. Find people with alternative points of view, and listen to them so every perspective can be understood. This can help form a well-rounded opinion.

When possible, solicit feedback in the open. When decision-making is transparent, four important advantages emerge: (1) people who care about the decision can get involved, which makes the decision more thoughtful and increases the probability of the decision being right, (2) space is created for a diversity of perspective, which increases the potential to hear from voices that can be overlooked, (3) people with varying levels of experience can contribute and learn from each other, and (4) a public record exists that documents why the decision was made.

After practicing critical listening, the second step is to act decisively. Once a decision is made, document the reasoning, including potential drawbacks. This helps assure people who don't agree with the decision that their voices have still been heard.

When acting decisively, it’s important to distinguish between reversible and irreversible decisions. Decisions that are difficult to overturn should be made carefully and methodically to increase the probability of making the right decision. Decisions that are easy to reverse, however, can be made much more quickly. These decisions can be made without listening to every possible viewpoint. When reversible decisions are made in a leisurely fashion, innovation tapers and communities become risk averse, stifling experimentation.

In rare cases, difficult decisions have to be made unilaterally, especially in situations where information can't be shared publicly. Unilateral decisions should be avoided, because it is unclear why the decision was made. They can breed resentment, damage trust, and lower morale.

#### Principle 5: Everyone has something to contribute

Everyone who uses Drupal benefits from work that thousands of other individuals and organizations have contributed. Drupal is great because it is continuously improved by a diverse community of contributors who are enthusiastic to give back.

Giving back can include contributing code, promoting Drupal, providing financial support, writing documentation, organizing events, mentoring others, answering support questions, and more. Even something as small as telling a best friend about Drupal is a valued contribution.

Understand and respect that some can give more than others and that some might not be able to give back at all. Our goal is not to foster an environment that demands what and how others should give back. It is to foster an environment worthy of contribution.

Don't take contributions for granted. Understand and acknowledge that behind every contribution is a person or organization who decided to contribute their time, talent or money to make the project better.

#### Principle 6: Choose to lead

Leadership is not something that is appointed; it is a role that any individual can take on. I invite you to discover how you can help lead a part of Drupal.

Leadership is about having positive influence, not about having a specific title or authority. If you have the capacity and desire to influence others or drive change, you can be a leader.

Leadership is also not dependent on having the greatest knowledge of software or technical skills. Instead, great leaders care deeply about others and understand the importance of putting the project first.

Recognize that leadership is learned over time, often from an accumulation of challenging experiences. The expectation of leaders is not that they are perfect or have years of experience. The expectation of leaders is that they learn from their mistakes, rise to the challenge, support others ahead of their own needs or ego, and continuously work to improve themselves.

Everyone has heard stories about companies where employees show up to work, do the minimum, assume no responsibility, and collect a paycheck. Employees end up finding little purpose at work and simply go through the motions; the result is that companies grow stagnant.

I fundamentally believe people do their best work when they are empowered to take on ownership and responsibility. I've personally invited many people to take ownership over parts of Drupal, and I encourage you to do the same within our community.

### Strive for excellence

#### Principle 7: Embrace change

The Drupal community has long shared the saying, "The drop is always moving," which celebrates a commitment to embrace change.

Drupal has been successful because the community hasn’t been afraid to make big, forward-looking changes. In fact, the Drupal community’s ability to challenge the status quo and remain resilient makes Drupal one of the few content management systems that has stayed relevant for this long.

One of the biggest risks to the project is when the community refrains from change. It’s important to continue to re-evaluate every aspect of Drupal and look for ways to improve it. See change as the beginning of an exciting new chapter and have faith in the adventure that follows.

It’s also important to take time to celebrate. Take a moment to be proud and apply renewed enthusiasm to the next project.

### Treat each other with dignity and respect

#### Principle 8: Every person is welcome; every behavior is not

The Drupal community is a diverse group of people from a wide variety of backgrounds. Supporting diversity, equity, and inclusion is important not just because it is the right thing to do but because it is essential to the health and success of the project. The people who work on the Drupal project should reflect the diversity of people who use and work with the software. In order to do this, our community needs to build and support local communities and Drupal events in all corners of the world. Prioritizing accessibility and internationalization is an important part of this commitment.

The expectation of the entire Drupal community is to be present and to promote dignity and respect for all community members. People in our community should take responsibility for their words and actions and the impact they have on others.

Our community is, by default, accepting with one exception: we will not accept intolerance. Every person is welcome, but every behavior is not. Our community promotes behaviors and decisions that support diversity, equity, and inclusion and reduce hatred, oppression, and violence. We believe that safety is an important component of dignity and respect, and we encourage behaviors that keep our community members safe. Our Code of Conduct is designed to help communicate these expectations and to help people understand where they can turn for support when needed.

#### Principle 9: Be constructively honest, and relentlessly optimistic

When delivering my Driesnote at DrupalCon, I always try to be honest and accurately evaluate the state of Drupal. It is a long-standing tradition to highlight both the good and the bad. For many, especially those new to Drupal, highlighting our weaknesses in my opening keynote seems strange, because it is not something our competitors do - in fact, it could help them compete against Drupal!

I’ve always believed that everyone in our community should understand our biggest challenges so they can help solve them. I try to be constructive and encourage a positive path forward. I'm relentlessly optimistic about Drupal's future.

You can also be constructive, honest, and optimistic when giving day-to-day feedback. For example, when reviewing each other's contributions, we should evaluate them accurately and give kind but honest feedback. For many, honesty can be difficult to stomach. You can be optimistic and supportive by giving suggestions for how to improve their contributions. By being helpful, you encourage people to accept feedback and act on it.

#### Principle 10: Seek first to understand, then to be understood

Assume the best of other contributors and suspend judgement until you have invested time to understand their decisions, ask questions, and listen.

When people feel frustrated about certain decisions, the natural instinct is to assume that the other person is wrong. Before expressing a disagreement, make a serious attempt to understand the reasons behind the decision. More often than not, it becomes clear that those in disagreement only knew part of the story.

Many people listen with the intent to reply, not with the intent to understand; instead they want to be understood and get their point across. They filter what they hear through their own life experiences or frame of reference. Do the opposite: seek first to understand, then to be understood. Gaining understanding requires empathy and a desire to understand with the intent to help.

This principle is also applicable to how our community processes feedback. When receiving constructive feedback, keep an open mind. It's easier said than done, but be growth-minded and look at feedback as an opportunity to learn and grow. Don't fear that embracing feedback highlights weaknesses — use feedback as a way to pivot, improve and learn from each other.

Few things are as powerful as having strong conviction about your ideas. While having conviction often creates momentum, rigid beliefs can impede even brilliant individuals. No matter how smart, focused, or talented the individual, community members will likely struggle if they chose to prioritize their own beliefs over the right approach. The ability to realize when you’re wrong, embrace a better idea, and to adapt is key to winning. Let’s be open-minded and let go of poor ideas faster than our competition.

A lot of this principle comes from the highly recommended book, "The 7 Habits of Highly Effective People." This principle is not specific to Drupal. It's a skill that is valuable for everyone and is critical to good decision-making anywhere, whether you are a developer, a project manager, a team leader, or a parent.

### Enjoy what you do

#### Principle 11: Be sure to have fun

Sharing knowledge, empowering others, and working together to overcome challenging hurdles are all gratifying experiences. Along the way, the generosity, kindness, and fellowship shared by the Drupal community can also be rewarding in unexpected ways.

Wherever your interest lies, or however you choose to contribute and get involved, spend time enjoying what you do. When every member of Drupal is empowered to follow their passion, the community becomes happier and healthier. Grow, learn, share, and make friends along the way. Above all else, be sure to have fun.

## Evolving and governing our principles

Good principles are robust, and I encourage everyone to debate these principles to help make them stronger.

It is inevitable that we'll discover that some principles conflict with one another or that certain principles are missing. Please raise these potential issues and trust that I will be seeking to understand your criticisms and embracing change as our principles evolve over time.

For now, I'll act as the maintainer of our principles by committing them to the governance Git repository so that others can submit patches at <https://www.drupal.org/project/issues/governance>.
